<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Исусья тряпка</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 20px;
        padding-bottom: 40px;
      }

      /* Custom container */
      .container-narrow {
        margin: 0 auto;
        max-width: 1000px;
      }
      .container-narrow > hr {
        margin: 30px 0;
      }

      /* Main marketing message and sign up button */
      .jumbotron {
        margin: 10px 0;
        text-align: center;
      }
      .jumbotron h1 {
        font-size: 72px;
        line-height: 1;
      }
      .jumbotron .btn {
        font-size: 21px;
        padding: 14px 24px;
      }

      /* Supporting marketing content */
      .marketing {
        margin: 60px 0;
      }
      .marketing p + h4 {
        margin-top: 28px;
      }
      .va {
        padding-top: 50px;
      }
    </style>
    <link href="assets/css/bootstrap-responsive.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="assets/js/html5shiv.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
                    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
                                   <link rel="shortcut icon" href="assets/ico/favicon.png">
  </head>

  <body>

    <div class="container-narrow">
      <div class="jumbotron">
        <h1>Исусья тряпка!</h1>
        <hr>
        <div class="row-fluid well">
  <div class="span8"><iframe width="560" height="315" src="//www.youtube.com/embed/3IxdCD9_RnQ" frameborder="0" allowfullscreen></iframe>
  </div>
  <div class="span4">
          <p class="lead">Вы скажете "О, Господи! Куда мир катится!?", когда будете вытирать ею все вокруг! </p>
          <div class="row-fluid">
            <div class="span6">
              <img src="assets/img/price.png" width="150" alt=""/>
            </div>
            <div class="span6">
              <p class="va">
              <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_top">
<input type="hidden" name="cmd" value="_s-xclick">
<input type="hidden" name="hosted_button_id" value="AWQWQGYUXPPMU">
<input type="image" src="https://www.sandbox.paypal.com/ru_RU/i/btn/btn_buynow_LG.gif" border="0" name="submit" alt="PayPal — более безопасный и легкий способ оплаты через Интернет!">
<img alt="" border="0" src="https://www.sandbox.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">
</form>

</p>
            </div>
          </div>
        </div>
</div>
      </div>
      <h2>Почему именно Исусья тряпка?</h2>
      <div class="row-fluid marketing">
        <div class="span12">
          <h4>
            <i class="icon-ok"></i>
            Вы легко сможете скрыть следы убийства в квартире, на машине, на яхте, в троллейбусе!
          </h4>

          <h4>
            <i class="icon-ok"></i>
            Решили утопить Исусью тряпку? А вот хрен вам! У нее синдром Моисея!
          </h4>

          <h4>
            <i class="icon-ok"></i>
            С помощью этой тряпки Вы можете воровать одежду!
          </h4>

          <h4>
            <i class="icon-ok"></i>
            Резать!
          </h4>

          <h4>
            <i class="icon-ok"></i>
            Прикрывать мину!
          </h4>
          <h4>
            <i class="icon-ok"></i>
            Практиковать каннибализм и живодёрство!
          </h4>
        </div>
      </div>
      <h2>У нас уже купили:</h2>
      <div class="row-fliud">
        <div clsss="span12">
          <blockquote>
            <p>Я избила им грудного ребенка!</p>
            <small>Авдотья Сидоровна</small>
          </blockquote>
        </div>
        <div clsss="span12">
          <blockquote>
            <p>Она вовсе не повлияла на мой вес!</p>
            <small>Маргарита Тимофеевна</small>
          </blockquote>
        </div>
        <div clsss="span12">
          <blockquote>
            <p>Нет уж, спасибо! Я такую хуйню покупать не буду! В пизду!</p>
            <small>Никанор Яковлевич</small>
          </blockquote>
        </div>
        <div clsss="span12">
          <blockquote>
            <p>Нам она заменила страпон!</p>
            <small>Две невинные тайские девушки))</small>
          </blockquote>
        </div>
        
      </div>

      <hr>

      <div class="footer">
        <p>&copy; Company 2013</p>
      </div>

    </div> <!-- /container -->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap-transition.js"></script>
    <script src="assets/js/bootstrap-alert.js"></script>
    <script src="assets/js/bootstrap-modal.js"></script>
    <script src="assets/js/bootstrap-dropdown.js"></script>
    <script src="assets/js/bootstrap-scrollspy.js"></script>
    <script src="assets/js/bootstrap-tab.js"></script>
    <script src="assets/js/bootstrap-tooltip.js"></script>
    <script src="assets/js/bootstrap-popover.js"></script>
    <script src="assets/js/bootstrap-button.js"></script>
    <script src="assets/js/bootstrap-collapse.js"></script>
    <script src="assets/js/bootstrap-carousel.js"></script>
    <script src="assets/js/bootstrap-typeahead.js"></script>

  </body>
</html>
